#ifndef DEVICEGRAPHICSITEM_H
#define DEVICEGRAPHICSITEM_H

#include <QGraphicsEllipseItem>

class Device;
class DeviceGraphicsItem : public QObject, public QGraphicsEllipseItem
{
    Q_OBJECT
public:
    DeviceGraphicsItem(Device* device, QGraphicsItem* parent = nullptr);
signals:
    void itemSelectChanged(const Device* dev);
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;
private:
    Device* _device;
};

#endif // DEVICEGRAPHICSITEM_H
