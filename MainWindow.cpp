#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "DeviceModel.h"
#include "DeviceScene.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    DeviceModel* model = new DeviceModel();
    ui->treeView->setModel(model);

    DeviceScene* scene = new DeviceScene();
    scene->setModel(model);
    ui->graphicsView->setScene(scene);

    connect(scene, &DeviceScene::itemSelectChanged, model, &DeviceModel::deviceSelectChanged);
    connect(ui->treeView, &DeviceTreeView::leafDoubleClicked, [&, model, scene](const QModelIndex& index) {
        model->toggleDeviceSelection(index);
        scene->update();
    });

    ui->treeView->expandAll();
}

MainWindow::~MainWindow()
{
    delete ui;
}

