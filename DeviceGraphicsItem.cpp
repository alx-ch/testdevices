#include "DeviceGraphicsItem.h"
#include "DeviceModel.h"
#include <QPainter>

DeviceGraphicsItem::DeviceGraphicsItem(Device* device, QGraphicsItem *parent) :
    QGraphicsEllipseItem(QRectF(0, 0, 20, 20), parent), _device(device)
{
}

void DeviceGraphicsItem::mousePressEvent(QGraphicsSceneMouseEvent */*event*/)
{
    this->_device->selected = !this->_device->selected;
    emit itemSelectChanged(_device);
    update(boundingRect());
}

void DeviceGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setRenderHint(QPainter::Antialiasing);
    if (_device) {
        setPen(QPen(_device->selected ? Qt::red: Qt::gray, 2));
        setBrush(QBrush(_device->selected ? QColor(Qt::red).lighter(175): Qt::white));
    }
    QGraphicsEllipseItem::paint(painter, option, widget);
}

