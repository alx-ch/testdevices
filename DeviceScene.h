#ifndef DEVICESCENE_H
#define DEVICESCENE_H

#include <QGraphicsScene>

class DeviceModel;
class Device;
class DeviceScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit DeviceScene(QObject *parent = nullptr);
    void setModel(DeviceModel* model);
signals:
    void itemSelectChanged(const Device* _dev);
private:
    DeviceModel* _model;
};

#endif // DEVICESCENE_H
