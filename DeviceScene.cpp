#include "DeviceScene.h"
#include "DeviceModel.h"
#include "DeviceGraphicsItem.h"

DeviceScene::DeviceScene(QObject *parent)
    : QGraphicsScene(parent) {
}

void DeviceScene::setModel(DeviceModel *model)
{
    clear();

    _model = model;

    int x= 0, y = 0;
    for(const DeviceGroup* group: _model->groups()) {
        for(Device* device: group->children) {
            DeviceGraphicsItem* deviceItem = new DeviceGraphicsItem(device);
            deviceItem->setPos( QPointF(x += 20, y) );

            connect(deviceItem, &DeviceGraphicsItem::itemSelectChanged, this, &DeviceScene::itemSelectChanged);

            addItem(deviceItem);
        }
    }
}
