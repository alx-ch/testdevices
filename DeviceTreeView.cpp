#include "DeviceTreeView.h"
#include <QMouseEvent>

DeviceTreeView::DeviceTreeView(QWidget *parent) :
    QTreeView(parent)
{
}

void DeviceTreeView::mouseDoubleClickEvent(QMouseEvent *event)
{
    QModelIndex index = indexAt(event->pos());
    if (index.parent().isValid()) {
        emit leafDoubleClicked(index);
    }

    QTreeView::mouseDoubleClickEvent(event);
}
