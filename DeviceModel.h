#ifndef DEVICEMODEL_H
#define DEVICEMODEL_H

#include <QAbstractItemModel>
#include <QBrush>
#include <QPen>

struct DeviceGroup;

enum class Type {
    Device,
    DeviceGroup
};

struct Base {
    Base(const QString& name = QString()) {
        this->name = name;
    }
    QString name;
    virtual Type type() const = 0;
};


struct Device : public Base {
    Device(const QString& name = ""):
        Base(name), parent(nullptr) {
    }

    QString address = "adr_";
    bool selected = false;

    DeviceGroup* parent;

    Type type() const override {
        return Type::Device;
    }
    int idx() const;    
    bool operator == (const Device* second);
};


struct DeviceGroup : public Base {
    DeviceGroup(const QString& name = ""): Base(name) {
    }

    ~DeviceGroup() {
        qDeleteAll(children);
    }

    bool cheched = false;
    QList<Device*> children;

    Type type() const override {
        return Type::DeviceGroup;
    }
    DeviceGroup* addChild(Device* device);
    bool operator == (const DeviceGroup* group2);
};



#include <QDebug>
class DeviceModel : public QAbstractItemModel
{
public:
    DeviceModel(QObject* parent = nullptr): QAbstractItemModel(parent) {
        _list << (new DeviceGroup("group 1"))
                 ->addChild(new Device("Device 1.1"))
                 ->addChild(new Device("Device 1.2"))
                 ->addChild(new Device("Device 1.3"))
                 ->addChild(new Device("Device 1.4"))
                 ->addChild(new Device("Device 1.5"))
                 ->addChild(new Device("Device 1.6"));

        _list << (new DeviceGroup("group 2"))
                 ->addChild(new Device("Device 2.1"))
                 ->addChild(new Device("Device 2.2"))
                 ->addChild(new Device("Device 2.3"))
                 ->addChild(new Device("Device 2.4"))
                 ->addChild(new Device("Device 2.5"))
                 ->addChild(new Device("Device 2.6"));
    }

    ~DeviceModel() {
        qDeleteAll(_list);
    }

    QModelIndex index(int row, int column, const QModelIndex &parent) const override {
        if (!hasIndex(row, column, parent))
            return QModelIndex();

        if (!parent.isValid()) {
            return createIndex(row, column, _list[row]);
        }

        Base* typed = static_cast<Base*>(parent.internalPointer());
        if (typed->type() == Type::DeviceGroup && idxInRange(row, static_cast<DeviceGroup*>(typed))) {
            Device* device = static_cast<DeviceGroup*>(typed)->children[row];
            return createIndex(row, column, device);
        }

        return QModelIndex();
    }

    QModelIndex parent(const QModelIndex &child) const override {
        if (!child.isValid())
            return QModelIndex();

        Base* typed = static_cast<Base*>(child.internalPointer());
        if (typed->type() == Type::Device) {
            Device* device = static_cast<Device*>(typed);
            int row = _list.indexOf(device->parent);
            return createIndex(row, 0, device->parent);
        }


        return QModelIndex();
    }

    int rowCount(const QModelIndex &parent) const override {
        if (parent.column() > 0)
            return 0;

        if (!parent.isValid())
            return _list.count();

        Base* data = static_cast<Base*>( parent.internalPointer() );
        if (data->type() == Type::DeviceGroup) {
            return static_cast<DeviceGroup*>(data)->children.count();
        }

        return 0;
    }

    int columnCount(const QModelIndex &parent) const override {
        (void) parent;
        return 3;
    }

    QVariant data(const QModelIndex &index, int role) const override {
        if (index.isValid()) {
            if (index.parent().isValid()) {
                Device* device = static_cast<Device*>(index.internalPointer());
                if (role == Qt::DisplayRole) {
                    if (index.column() == 0)
                        return device->name;
                    if (index.column() == 2)
                        return device->address;
                }

                if (role == Qt::BackgroundRole) {
                    if (index.column() == 0)
                        return device->selected ? QBrush(Qt::lightGray): QBrush(Qt::white);
                }
                if (role == Qt::ForegroundRole) {
                    if (index.column() == 0)
                        return device->selected ? QBrush(Qt::red): QBrush(Qt::black);
                }
            } else {
                DeviceGroup* group = static_cast<DeviceGroup*>(index.internalPointer());
                if ( role == Qt::CheckStateRole && index.column() == 1 )
                    return group->cheched ? Qt::Checked : Qt::Unchecked;

                if (role == Qt::DisplayRole) {
                    if (index.column() == 0)
                        return group->name;
                }
            }
        }
        return QVariant();
    }

    Qt::ItemFlags flags(const QModelIndex &index) const override {
        if (index.isValid()) {
            Qt::ItemFlags flags = QAbstractItemModel::flags(index);
            if (index.column() == 1) {
                return flags | Qt::ItemFlag::ItemIsUserCheckable;
            }
            return flags;
        }
        return Qt::ItemFlags();
    }

    const QList<DeviceGroup*>& groups() const {
        return this->_list;
    }

public slots:
    void deviceSelectChanged(const Device* device);
    void toggleDeviceSelection(const QModelIndex& index);

private:
    bool idxInRange(int idx, DeviceGroup* group = nullptr) const {
        if (group) {
            return idx > -1 && idx < group->children.count();
        }
        return idx > -1 && idx < _list.count();
    }
    QList<DeviceGroup*> _list;
};

#endif // DEVICEMODEL_H
