#ifndef DEVICETREEVIEW_H
#define DEVICETREEVIEW_H

#include <QTreeView>

class QMouseEvent;
class DeviceTreeView : public QTreeView
{
    Q_OBJECT
public:
    DeviceTreeView(QWidget* parent = nullptr);
signals:
    void leafDoubleClicked(const QModelIndex& index);
protected:
    void mouseDoubleClickEvent(QMouseEvent *event) override;
};

#endif // DEVICETREEVIEW_H
