#include "DeviceModel.h"

int Device::idx() const
{
    if (parent)
        return parent->children.indexOf(this);
    return -1;
}

bool Device::operator == (const Device *second) {
    return this->name.compare(second->name) == 0;
}

DeviceGroup *DeviceGroup::addChild(Device *device)
{
    device->parent = this;
    this->children << device;
    return this;
}

bool DeviceGroup::operator == (const DeviceGroup* group2) {
    return this->name.compare(group2->name) == 0;
}

void DeviceModel::deviceSelectChanged(const Device *device)
{
    int row = device->idx();
    QModelIndex index = createIndex(row, 0, device);
    emit dataChanged(index, index);
}

void DeviceModel::toggleDeviceSelection(const QModelIndex &index)
{
    Base* base = static_cast<Base*>(index.internalPointer());
    if (base->type() == Type::Device) {
        Device* device = static_cast<Device*>(base);
        device->selected = !device->selected;
        emit dataChanged(index, index);
    }
}
